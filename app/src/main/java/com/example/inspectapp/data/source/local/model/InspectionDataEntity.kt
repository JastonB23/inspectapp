package com.example.inspectapp.data.source.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "Inspections")
class InspectionDataEntity @JvmOverloads constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int,
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "score") var score: Int = 0,
    @ColumnInfo(name = "type") var type: String = "",
    @ColumnInfo(name = "dateTime") var dateTime: Long = 0
)