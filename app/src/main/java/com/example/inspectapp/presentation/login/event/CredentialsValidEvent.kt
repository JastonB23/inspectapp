package com.example.inspectapp.presentation.login.event

import com.example.inspectapp.presentation.core.Event

class CredentialsValidEvent<T>(private val content: (T)) : Event<T>(content)