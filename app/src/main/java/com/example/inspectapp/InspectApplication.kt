package com.example.inspectapp

import android.app.Application
import com.example.inspectapp.data.IRepository
import com.example.inspectapp.data.RepositoryProvider

class InspectApplication : Application() {
    val repository: IRepository
        get() = RepositoryProvider.provideRepository(this)
}