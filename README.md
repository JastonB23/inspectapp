# Introduction

*A Plug-in-and-Play app, that allows registered inspectors to complete and submit inspection reports.*

### Functional Requirements

* The application is designed to allow inspectors to complete inspections using a form based system to record the results.

* Users must be authenticated before they can access the application, and their username an password will be encrypted and stored on the device to ease future logins after successfully authenticating for the first time.

* Each question displays a set of available answers for the user to select a pre-defined value, or an input mechanism to specify a desired value.

* Every form can downloaded to the devices internal memory to complete and submit when offline or save whilst in progress.

* Completed forms can be submitted when the device is online, or stored locally within the device when offline and synced when network access becomes available again.

* The application will display the final score of each completed inspections.

* User can request the details of past inspections, and any future inspections.

### Non-Functional Requirements

* The application can be downloaded from the Google Play Store.

* The application can be run on Android phone and Tablet devices.

* Inspection forms should load in no less than 3 seconds.

* Submitted forms should be processed within 10 seconds.

* Previously submitted form request should be processed within 10 seconds.

* Data stored within the device should be stored within a relational database structure.

# Data Layer

### Entity Relation Diagram

The following diagram illustrates how each entity utilised by the application is related and structured within the devices relational database:

![ERD](/assets/Inspect_ERD.png)

### Stored and Cached Data

All data stored locally on any device will be encrypted using the Google encryption library Tink. As primitive encryption is not the most effective means to encrypt data and is also harder to maintain.

[Tink](https://github.com/google/tink)

All form related data will be cached and stored within a relational database in the device internal memory.

This will allow users to complete their inspections even if their is no internet connectivity. Once network connectivity has been re-established,
the application will use [Work Manager](https://developer.android.com/topic/libraries/architecture/workmanager) to submit any completed and saved forms to the back-end servers and then remove them from the devices internal memory.

# Authentication Layer

The application will use the Firebase Authentication to authenticate users. This solution provides authentication using passwords, phone numbers, popular federated identity providers like Google, Facebook and Twitter, and others.

Firebase Authentication provides a server-less, out-of-the-box back-end, which is free to use, integrates well with Android and iOS apps and implements industry standard protocols like OAuth 2.0 and OpenID Connect.

# Presentation Layer

The application will follow Material Design guidelines for the UX and all UI components implemented will be native Material Design components.

# Dependencies

* Firebase Authentication SDK
* Retrofit
* Material Design
* Kotlin Coroutines
* Room
* Dagger2 + Hilt
* Google Tink

# System Requirements

* Android 7.0 or higher



