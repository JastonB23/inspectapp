package com.example.inspectapp.data.source

import com.example.inspectapp.domain.model.Inspection
import com.example.inspectapp.data.Result

interface IDataSource {

    suspend fun getInspections(): Result<List<Inspection>>

    suspend fun getInspections(inspectionId: String): Result<Inspection>

    suspend fun insertInspection(inspection: Inspection)

    suspend fun deleteAllInspections()
}