package com.example.inspectapp.data.network

fun getHistoricInspectionsJSON(): String {
    return "[" +
     "{\"id\": \"1\",\"name\": \"Test Inspection 1\",\"score\": 75,\"type\": \"Type A\",\"datetime\": 1598107084 }," +
     "{\"id\": \"2\",\"name\": \"Test Inspection 2\",\"score\": 75,\"type\": \"Type B\",\"datetime\": 1598107084 }," +
     "{\"id\": \"3\",\"name\": \"Test Inspection 3\",\"score\": 45,\"type\": \"Type B\",\"datetime\": 1598107084 }," +
     "{\"id\": \"4\",\"name\": \"Test Inspection 4\",\"score\": 34,\"type\": \"Type A\",\"datetime\": 1598107084 }," +
     "{\"id\": \"5\",\"name\": \"Test Inspection 5\",\"score\": 78,\"type\": \"Type C\",\"datetime\": 1598107084 }," +
     "{\"id\": \"6\",\"name\": \"Test Inspection 6\",\"score\": 99,\"type\": \"Type A\",\"datetime\": 1598107084 }," +
     "{\"id\": \"7\",\"name\": \"Test Inspection 7\",\"score\": 35,\"type\": \"Type D\",\"datetime\": 1598107084 }," +
     "{\"id\": \"8\",\"name\": \"Test Inspection 8\",\"score\": 21,\"type\": \"Type D\",\"datetime\": 1598107085 }," +
     "{\"id\": \"9\",\"name\": \"Test Inspection 9\",\"score\": 56,\"type\": \"Type D\",\"datetime\": 1598107086 }," +
     "{\"id\": \"10\",\"name\": \"Test Inspection 10\",\"score\": 100,\"type\": \"Type D\",\"datetime\": 1598107087 }" +
    "]"
}