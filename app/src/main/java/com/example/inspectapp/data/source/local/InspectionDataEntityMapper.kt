package com.example.inspectapp.data.source.local

import com.example.inspectapp.data.source.local.model.InspectionDataEntity
import com.example.inspectapp.domain.IMapper
import com.example.inspectapp.domain.model.Inspection

class InspectionDataEntityMapper : IMapper<InspectionDataEntity, Inspection> {
    override fun map(from: InspectionDataEntity): Inspection {
        return Inspection(
            id = from.id,
            name = from.name,
            type = from.type,
            score = from.score,
            datetime = from.dateTime
        )
    }

    override fun mapTo(to: Inspection): InspectionDataEntity {
        return InspectionDataEntity(
            id = to.id,
            name = to.name,
            type = to.type,
            score = to.score,
            dateTime = to.datetime
        )
    }
}