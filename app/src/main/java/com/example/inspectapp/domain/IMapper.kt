package com.example.inspectapp.domain

interface IMapper<FROM, TO> {
    fun map(from: FROM): TO
    fun mapTo(to: TO): FROM
}