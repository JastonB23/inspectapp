package com.example.inspectapp.domain.usecases

import com.example.inspectapp.data.IRepository
import com.example.inspectapp.domain.Result
import com.example.inspectapp.data.Result.*

open class GetHistoricInspectionsUsecase constructor(
    private val repository: IRepository
) {
    suspend fun execute(forceSync: Boolean = true): Result {
        val result = repository.getInspections(forceSync)
        return if (result is Success) {
            Result.Success(result.data)
        } else Result.Error
    }
}