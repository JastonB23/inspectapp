package com.example.inspectapp.data.source

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.inspectapp.data.source.local.InspectionDataDao
import com.example.inspectapp.data.source.local.model.InspectionDataEntity

@Database(entities = [InspectionDataEntity::class], version = 1, exportSchema = false)
abstract class InspectionDatabase : RoomDatabase() {
    abstract fun inspectionDataDao() : InspectionDataDao
}