package com.example.inspectapp.domain.model

data class Inspection(
    val id: Int,
    val name: String,
    val score: Int,
    val type: String,
    val datetime: Long
)