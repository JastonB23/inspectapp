package com.example.inspectapp.presentation.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.inspectapp.presentation.core.Event
import com.example.inspectapp.presentation.login.event.CredentialsValidEvent
import com.example.inspectapp.presentation.login.event.PasswordInvalidEvent
import com.example.inspectapp.presentation.login.event.UsernameInvalidEvent

class LoginViewModel : ViewModel() {
    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private val _openDashboardEvent = MutableLiveData<Event<Nothing?>>()
    val openDashboardEvent: LiveData<Event<Nothing?>> = _openDashboardEvent

    fun onLoginClicked() {
        val event = if (isUsernameInvalid()) {
            UsernameInvalidEvent(null)
        } else if (isPasswordInvalid()) {
            PasswordInvalidEvent(null)
        } else {
            CredentialsValidEvent(null)
        }

        _openDashboardEvent.value = event
    }

    private fun isUsernameInvalid(): Boolean {
        return username.value.isNullOrEmpty()
    }

    private fun isPasswordInvalid(): Boolean {
        return password.value?.length!! < 8
    }
}