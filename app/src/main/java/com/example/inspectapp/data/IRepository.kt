package com.example.inspectapp.data

import com.example.inspectapp.domain.model.Inspection

interface IRepository {
    suspend fun getInspections(forceSync: Boolean = true): Result<List<Inspection>>
}