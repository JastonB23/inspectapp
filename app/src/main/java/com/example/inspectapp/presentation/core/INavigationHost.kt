package com.example.inspectapp.presentation.core

import androidx.fragment.app.Fragment

interface INavigationHost {
    fun navigateTo(fragment: Fragment, addToBackstack: Boolean)
}