package com.example.inspectapp.presentation.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.inspectapp.R
import com.example.inspectapp.databinding.FragmentLoginBinding
import com.example.inspectapp.presentation.core.EventObserver
import com.example.inspectapp.presentation.core.INavigationHost
import com.example.inspectapp.presentation.dashboard.DashboardFragment
import com.example.inspectapp.presentation.login.event.PasswordInvalidEvent
import com.example.inspectapp.presentation.login.event.UsernameInvalidEvent

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    private lateinit var loginViewModel: LoginViewModel

    companion object {
        const val TAG = "Login"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        binding = FragmentLoginBinding.inflate(inflater, container, false).apply {
            viewmodel = loginViewModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.usernameEditText.setOnKeyListener{ _, _, _ ->
            binding.usernameTextLayout.error = null
            false
        }

        binding.passwordEditText.setOnKeyListener{ _, _, _ ->
            binding.passwordTextInput.error = null
            false
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.lifecycleOwner = this.viewLifecycleOwner
        setupNavigationObservers()
    }

    private fun setupNavigationObservers() {
        loginViewModel.openDashboardEvent.observe(this.viewLifecycleOwner, EventObserver {event ->
            when(event) {
                is PasswordInvalidEvent -> binding.passwordTextInput.error = getString(R.string.password_error)
                is UsernameInvalidEvent -> binding.usernameTextLayout.error = getString(R.string.username_error)
                else -> (activity as INavigationHost).navigateTo(DashboardFragment(), false)
            }
        })
    }
}