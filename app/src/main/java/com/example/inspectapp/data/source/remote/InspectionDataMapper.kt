package com.example.inspectapp.data.source.remote

import com.example.inspectapp.data.source.local.model.InspectionDataEntity
import com.example.inspectapp.data.source.remote.model.InspectionData
import com.example.inspectapp.domain.IMapper
import com.example.inspectapp.domain.model.Inspection

class InspectionDataMapper: IMapper<InspectionData, Inspection> {

    override fun map(from: InspectionData): Inspection {
        return Inspection(
            id = from.id,
            name = from.name,
            type = from.type,
            score = from.score,
            datetime = from.datetime
        )
    }

    override fun mapTo(to: Inspection): InspectionData {
        return InspectionData(
            id = to.id,
            name = to.name,
            type = to.type,
            score = to.score,
            datetime = to.datetime
        )
    }
}