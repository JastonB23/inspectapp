package com.example.inspectapp.presentation.dashboard.model

import com.example.inspectapp.domain.model.Inspection
import com.example.inspectapp.domain.IMapper

class InspectionPresentationMapper: IMapper<Inspection, InspectionPresentation> {

    override fun map(from: Inspection): InspectionPresentation {
        return InspectionPresentation(
            id = from.id,
            name = from.name,
            score = from.score,
            type = from.type,
            datetime = from.datetime
        )
    }

    override fun mapTo(to: InspectionPresentation): Inspection {
        return Inspection(
            id = to.id,
            name = to.name,
            type = to.type,
            score = to.score,
            datetime = to.datetime
        )
    }
}