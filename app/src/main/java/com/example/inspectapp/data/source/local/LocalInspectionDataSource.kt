package com.example.inspectapp.data.source.local

import com.example.inspectapp.data.Result
import com.example.inspectapp.data.source.IDataSource
import com.example.inspectapp.domain.model.Inspection
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LocalInspectionDataSource constructor(
    private val inspectionDataDao: InspectionDataDao,
    private val mapper: InspectionDataEntityMapper,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : IDataSource {

    override suspend fun getInspections(): Result<List<Inspection>> = withContext(ioDispatcher) {
        return@withContext try {
            val data = ArrayList<Inspection>()
            inspectionDataDao.getInspections().forEach {
                data.add(mapper.map(it))
            }
            Result.Success(data)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun getInspections(inspectionId: String): Result<Inspection> {
        TODO("Not yet implemented")
    }

    override suspend fun insertInspection(inspection: Inspection) {
        inspectionDataDao.insertInspection(mapper.mapTo(inspection))
    }

    override suspend fun deleteAllInspections() = withContext(ioDispatcher) {
       inspectionDataDao.deleteInspections()
    }
}