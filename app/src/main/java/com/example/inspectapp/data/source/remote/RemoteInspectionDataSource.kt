package com.example.inspectapp.data.source.remote

import com.example.inspectapp.data.network.InspectionsService
import com.example.inspectapp.data.network.getHistoricInspectionsJSON
import com.example.inspectapp.data.source.IDataSource
import com.example.inspectapp.data.Result
import com.example.inspectapp.domain.model.Inspection
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class RemoteInspectionDataSource constructor(
    private val service: InspectionsService,
    private val mapper: InspectionDataMapper
): IDataSource {

    override suspend fun getInspections(): Result<List<Inspection>> {
        return try {
            //TODO Uncomment when implementing remote API to get inspections data
            /*val data = service.getInspections().await().let {
                mapper.map(it)
            }*/

            val data = fromJson<List<Inspection>>(getHistoricInspectionsJSON())
            Result.Success(data)
        } catch (e: Exception) {
            e.printStackTrace()
            Result.Error(e)
        }
    }

    override suspend fun getInspections(inspectionId: String): Result<Inspection> {
        TODO("Not yet implemented")
    }

    override suspend fun insertInspection(inspection: Inspection) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteAllInspections() {
        TODO("Not yet implemented")
    }


    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object: TypeToken<T>(){}.type)
    }
}