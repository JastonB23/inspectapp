package com.example.inspectapp.presentation.core

import androidx.lifecycle.Observer

open class Event<out T>(private val content: T) {

    @Suppress("MemberVisibilityCanBePrivate")
    var hasBeenHandled = false
        private set // Allow external read but not write

    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }
}

class EventObserver<T>(private val onEventUnhandledContent: (Event<T>) -> Unit) : Observer<Event<T>> {
    override fun onChanged(event: Event<T>) {
        onEventUnhandledContent(event)
    }
}
