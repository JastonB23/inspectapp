package com.example.inspectapp.presentation.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.inspectapp.InspectApplication
import com.example.inspectapp.databinding.FragmentDashboardBinding
import com.example.inspectapp.presentation.core.ConnectivityProvider
import com.example.inspectapp.presentation.core.ConnectivityProvider.*
import com.example.inspectapp.presentation.core.ConnectivityProvider.NetworkState.*

class DashboardFragment : Fragment(), ConnectivityStateListener {

    private lateinit var dashboardBindingImpl: FragmentDashboardBinding
    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var adapter: DashboardAdapter
    private lateinit var connectivityProvider: ConnectivityProvider
    private var networkState: NetworkState = NotConnected

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel::class.java)

        dashboardBindingImpl = FragmentDashboardBinding.inflate(inflater, container, false).apply {
            viewmodel = dashboardViewModel
        }
        return dashboardBindingImpl.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dashboardBindingImpl.lifecycleOwner = this.viewLifecycleOwner

        connectivityProvider = ConnectivityProvider.createProvider(requireContext())

        setupListAdapter()

        setupRepository()

        dashboardViewModel.fetchHistoricInspections()
    }

    override fun onStart() {
        super.onStart()
        connectivityProvider.addListener(this)
    }

    override fun onStop() {
        super.onStop()
        connectivityProvider.removeListener(this)
    }

    override fun onStateChange(state: NetworkState) {
        networkState = state
        dashboardViewModel.setHasInternet(isNetworkConnected())
    }

    private fun setupListAdapter() {
        val viewModel = dashboardBindingImpl.viewmodel
        if (viewModel != null) {
            adapter = DashboardAdapter(viewModel)
            dashboardBindingImpl.inspectionsList.adapter = adapter
        } else {
            Log.d("DashboardFragment", "ViewModel not initialized when attempting to set up adapter.")
        }
    }

    private fun setupRepository() {
        val repository = (requireContext().applicationContext as InspectApplication).repository
        dashboardViewModel.setRepo(repository)
    }

    private fun isNetworkConnected() : Boolean {
        return networkState is Connected
    }
}