package com.example.inspectapp.presentation.dashboard

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.inspectapp.presentation.dashboard.model.InspectionPresentation

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<InspectionPresentation>) {
    (listView.adapter as DashboardAdapter).submitList(items)
}