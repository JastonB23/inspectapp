package com.example.inspectapp.data.source.remote.model

import com.google.gson.annotations.SerializedName

data class InspectionData (
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("score")
    val score: Int,
    @SerializedName("type")
    val type: String,
    @SerializedName("datetime")
    val datetime: Long
)