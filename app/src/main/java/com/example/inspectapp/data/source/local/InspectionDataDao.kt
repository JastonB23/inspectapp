package com.example.inspectapp.data.source.local

import androidx.room.*
import com.example.inspectapp.data.source.local.model.InspectionDataEntity

@Dao
interface InspectionDataDao {
    @Query("SELECT * FROM Inspections")
    suspend fun getInspections(): List<InspectionDataEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInspection(inspection: InspectionDataEntity)

    @Update
    suspend fun updateInspection(inspection: InspectionDataEntity): Int

    @Query("DELETE FROM Inspections WHERE id = :id")
    suspend fun deleteInspectionById(id: String): Int

    @Query("DELETE FROM Inspections")
    suspend fun deleteInspections()
}