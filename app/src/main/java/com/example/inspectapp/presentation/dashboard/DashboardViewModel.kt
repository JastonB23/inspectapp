package com.example.inspectapp.presentation.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.inspectapp.data.IRepository
import com.example.inspectapp.data.source.remote.InspectionDataMapper
import com.example.inspectapp.data.network.InspectionsService
import com.example.inspectapp.domain.Result
import com.example.inspectapp.domain.usecases.GetHistoricInspectionsUsecase
import com.example.inspectapp.presentation.dashboard.model.InspectionPresentation
import com.example.inspectapp.presentation.dashboard.model.InspectionPresentationMapper
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.CoroutineContext


class DashboardViewModel : ViewModel(), CoroutineScope {
    private val mapper = InspectionPresentationMapper()

    private lateinit var repository: IRepository
    private lateinit var usecase: GetHistoricInspectionsUsecase

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private val _items = MutableLiveData<List<InspectionPresentation>>().apply { value = emptyList() }
    val items: LiveData<List<InspectionPresentation>> = _items
    private val _hasInternet = MutableLiveData<Boolean>(false)

    val empty: LiveData<Boolean> = Transformations.map(_items) {
        it.isEmpty()
    }

    fun setHasInternet(hasInternet: Boolean) {
        _hasInternet.value = hasInternet
    }

    fun setRepo(repo: IRepository) {
        repository = repo
        usecase = GetHistoricInspectionsUsecase(repository)
    }

    fun fetchHistoricInspections() {
        launch {
            withContext(Dispatchers.IO) {
                when(val response = usecase.execute(_hasInternet.value!!)) {
                    is Result.Success -> {
                        val presentationData = response.data.map {
                            mapper.map(it)
                        }
                        _items.postValue(presentationData)
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}