package com.example.inspectapp.domain

import com.example.inspectapp.domain.model.Inspection

sealed class Result {
    data class Success(val data: List<Inspection>): Result()
    object Error: Result()
}