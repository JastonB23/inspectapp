package com.example.inspectapp.presentation.core

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Handler
import android.os.Looper
import com.example.inspectapp.presentation.core.ConnectivityProvider.NetworkState.*

class ConnectivityProvider(private val cm : ConnectivityManager) {
    companion object {
        fun createProvider(context: Context) : ConnectivityProvider {
            val cm = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            return ConnectivityProvider(cm)
        }
    }

    private val networkCallback = ConnectivityCallback()
    private val listeners = mutableSetOf<ConnectivityStateListener>()
    private var subscribed = false

    private inner class ConnectivityCallback : ConnectivityManager.NetworkCallback() {

        override fun onCapabilitiesChanged(network: Network, capabilities: NetworkCapabilities) {
            dispatchChange(Connected)
        }

        override fun onLost(network: Network) {
            dispatchChange(NotConnected)
        }
    }

    fun dispatchChange(state: NetworkState) {
        Handler(Looper.getMainLooper()).post {
            for (listener in listeners) {
                listener.onStateChange(state)
            }
        }
    }

    fun addListener(listener: ConnectivityStateListener) {
        listeners.add(listener)
        listener.onStateChange(getNetworkState()) // propagate an initial state
        verifySubscription()
    }

    fun removeListener(listener: ConnectivityStateListener) {
        listeners.remove(listener)
        verifySubscription()
    }

    private fun verifySubscription() {
        if (!subscribed && listeners.isNotEmpty()) {
            subscribe()
            subscribed = true
        } else if (subscribed && listeners.isEmpty()) {
            unsubscribe()
            subscribed = false
        }
    }

    private fun subscribe() {
        cm.registerDefaultNetworkCallback(networkCallback)
    }

    private fun unsubscribe() {
        cm.unregisterNetworkCallback(networkCallback)
    }

    private fun getNetworkState() : NetworkState {
        val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
        if (capabilities != null && capabilities.hasCapability(NET_CAPABILITY_INTERNET)) {
            return Connected
        }
        return NotConnected
    }

    sealed class NetworkState {
        object Connected : NetworkState()
        object NotConnected : NetworkState()
    }

    interface ConnectivityStateListener {
        fun onStateChange(state: NetworkState)
    }
}