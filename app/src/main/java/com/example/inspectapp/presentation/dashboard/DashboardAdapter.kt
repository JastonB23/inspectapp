package com.example.inspectapp.presentation.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.inspectapp.databinding.FragmentDashboardItemBinding
import com.example.inspectapp.presentation.dashboard.model.InspectionPresentation

class DashboardAdapter(private val viewModel: DashboardViewModel)
    : ListAdapter<InspectionPresentation, DashboardAdapter.DashboardViewHolder>(InspectionDiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardViewHolder {
        return DashboardViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: DashboardViewHolder, position: Int) {
        val item = getItem(position)

        holder.bind(viewModel, item)
    }

    class DashboardViewHolder private constructor(val binding: FragmentDashboardItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: DashboardViewModel, item: InspectionPresentation) {

            binding.viewmodel = viewModel
            binding.inspection = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): DashboardViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FragmentDashboardItemBinding.inflate(layoutInflater, parent, false)

                return DashboardViewHolder(binding)
            }
        }
    }
}

class InspectionDiffCallback : DiffUtil.ItemCallback<InspectionPresentation>() {
    override fun areItemsTheSame(
        oldItem: InspectionPresentation,
        newItem: InspectionPresentation
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: InspectionPresentation,
        newItem: InspectionPresentation
    ): Boolean {
        return oldItem == newItem
    }
}