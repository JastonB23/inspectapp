package com.example.inspectapp.data.network

import com.example.inspectapp.data.source.remote.model.InspectionData
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface InspectionsService {
    @GET("/historic_inspections")
    fun getInspections(): Deferred<List<InspectionData>>
}