package com.example.inspectapp.data

import android.content.Context
import androidx.room.Room
import com.example.inspectapp.data.network.InspectionsService
import com.example.inspectapp.data.source.IDataSource
import com.example.inspectapp.data.source.InspectionDatabase
import com.example.inspectapp.data.source.local.InspectionDataEntityMapper
import com.example.inspectapp.data.source.local.LocalInspectionDataSource
import com.example.inspectapp.data.source.remote.InspectionDataMapper
import com.example.inspectapp.data.source.remote.RemoteInspectionDataSource
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RepositoryProvider {
    private var database: InspectionDatabase? = null
    private var repository: IRepository? = null

    fun provideRepository(context: Context): IRepository {
        synchronized(this) {
            return repository ?: createRepository(context)
        }
    }

    private fun createRepository(context: Context): IRepository {
        return InspectionRepository(createRemoteDataSource(), createLocalDataSource(context))
    }

    private fun createRemoteDataSource(): IDataSource {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://test/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(InspectionsService::class.java)

        val mapper = InspectionDataMapper()

        return RemoteInspectionDataSource(service, mapper)
    }

    private fun createLocalDataSource(context: Context): IDataSource {
        val database = database ?: createDatabase(context)
        val mapper = InspectionDataEntityMapper()
        return LocalInspectionDataSource(database.inspectionDataDao(), mapper)
    }

    private fun createDatabase(context: Context): InspectionDatabase {
        val result = Room.databaseBuilder(
            context.applicationContext,
            InspectionDatabase::class.java, "Inspection.db"
        ).build()
        database = result
        return result
    }
}