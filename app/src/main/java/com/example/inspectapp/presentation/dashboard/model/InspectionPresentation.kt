package com.example.inspectapp.presentation.dashboard.model

import java.text.SimpleDateFormat
import java.util.*

data class InspectionPresentation(
    val id: Int,
    val name: String,
    val score: Int,
    val type: String,
    val datetime: Long
) {
    val formattedDateTime:String
        get() = getDateTime()

    private fun getDateTime(): String =
        try {
            val sdf = SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH)
            val netDate = Date(datetime)
            sdf.format(netDate)
        } catch (e: Exception) {
            "[Invalid]"
        }
}