package com.example.inspectapp.data

import com.example.inspectapp.data.source.IDataSource
import com.example.inspectapp.domain.model.Inspection
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.example.inspectapp.data.Result.*
import com.example.inspectapp.data.source.remote.model.InspectionData

class InspectionRepository(
    private val remoteDataSource: IDataSource,
    private val localDataSource: IDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : IRepository {

    override suspend fun getInspections(forceSync: Boolean): Result<List<Inspection>> {
        return withContext(ioDispatcher) {
            (fetchInspectionsFromRemoteOrLocal(forceSync) as? Success)?.let {
                return@withContext Success(it.data)
            }
            return@withContext Error(Exception("Illegal state"))
        }
    }

    private suspend fun fetchInspectionsFromRemoteOrLocal(forceUpdate: Boolean): Result<List<Inspection>> {
        return if (forceUpdate) {
            when (val remoteData = remoteDataSource.getInspections()) {
                is Success -> {
                    refreshLocalDataSource(remoteData.data)
                    remoteData
                }
                else -> throw IllegalStateException()
            }
        } else localDataSource.getInspections()
    }

    private suspend fun refreshLocalDataSource(inspections: List<Inspection>) {
        localDataSource.deleteAllInspections()
        inspections.forEach { inspection ->
            localDataSource.insertInspection(inspection)
        }
    }
}